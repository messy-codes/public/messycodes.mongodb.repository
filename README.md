Example for MongoDb repository pattern that based on the following articles https://www.davideguida.com/unit-testing-mongodb-in-c-part-1-the-repository/ 
written on top of .NET Standard 2.0, sample implementation on .NET Core 2.2 Web Api project.

Demo at Pivotal Web services : https://messycodesmongodbsampleaspnetcore.cfapps.io/swagger/index.html