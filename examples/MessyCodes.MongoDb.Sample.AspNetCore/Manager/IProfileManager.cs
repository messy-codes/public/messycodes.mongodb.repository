﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MessyCodes.MongoDb.Repositories;
using MessyCodes.MongoDb.Sample.AspNetCore.Model;

namespace MessyCodes.MongoDb.Sample.AspNetCore.Manager
{
    public interface IProfileManager
    {
        Task AddProfile(ProfileDTO profile);
        Task<IEnumerable<ProfileDTO>> GetProfile(string id = "");
        Task<bool> UpdateProfile(ProfileDTO profile);
        Task<bool> DeleteProfile(string id);
    }
}
