﻿using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MessyCodes.MongoDb.Sample.AspNetCore.Manager
{
    public interface IProfileAggregationManager
    {
        Task<long> CountProfile();
        Task<BsonDocument> CountGroupBy();
    }
}
