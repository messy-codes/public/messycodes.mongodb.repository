﻿using AutoMapper;
using MongoDB.Bson;
using MongoDB.Driver;
using MessyCodes.MongoDb.Context;
using MessyCodes.MongoDb.Sample.AspNetCore.Model;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MessyCodes.MongoDb.Sample.AspNetCore.Manager
{
    public class ProfileManager
        : IProfileManager
    {
        private readonly IDbContext _dbContext;
        private readonly IMapper _mapper;
        public ProfileManager(IDbContext dbContext, IMapper mapper)
        {
            _dbContext = dbContext ?? throw new ArgumentNullException("DbContext should not be null");
            _mapper = mapper ?? throw new ArgumentNullException("Mapper object should not be null");
        }

        public async Task AddProfile(ProfileDTO profile)
        {
            if (profile == null) throw new ArgumentNullException("Profile should not be null");

            await _dbContext.ProfileRepository.AddAsync(_mapper.Map<Entities.Profile>(profile));
        }

        public async Task<bool> DeleteProfile(string id)
        {
            if (string.IsNullOrEmpty(id)) throw new ArgumentNullException("Profile Id should not be null");

            var filterDefinition =
                Builders<Entities.Profile>
                    .Filter
                    .Eq(_ => _.Id, id);

            return
                await _dbContext.ProfileRepository.RemoveAsync(filterDefinition);
        }

        public async Task<IEnumerable<ProfileDTO>> GetProfile(string id)
        {
            var filterDefinitionBuilder =
                Builders<Entities.Profile>
                   .Filter;

            var filterDefinition = !string.IsNullOrEmpty(id) ? 
                filterDefinitionBuilder.Eq(_ => _.Id, id) : 
                filterDefinitionBuilder.Empty;

            var userProfile = (await _dbContext.ProfileRepository.GetAsync(filterDefinition))?.ToList();
            return 
                (userProfile == null) ? 
                new List<ProfileDTO>() : 
                _mapper.Map<List<Entities.Profile>, List<ProfileDTO>>(userProfile); 
        }

        public async Task<bool> UpdateProfile(ProfileDTO profile)
        {
            if (profile?.Id == null) throw new ArgumentNullException("Profile or ProfileId should not be null");

            var filterDefinition =
                Builders<Entities.Profile>
                    .Filter
                    .Eq(_ => _.Id, profile.Id);

            var updateDefinition =
                Builders<Entities.Profile>
                    .Update
                    .Set(_ => _.Firstname, profile.Firstname)
                    .Set(_ => _.Lastname, profile.Lastname)
                    .Set(_ => _.Email, profile.Email)
                    .Set(_ => _.Phoneno, profile.Phoneno);

            return
                await _dbContext.ProfileRepository.UpdateAsync(filterDefinition, updateDefinition);
        }
    }
}
