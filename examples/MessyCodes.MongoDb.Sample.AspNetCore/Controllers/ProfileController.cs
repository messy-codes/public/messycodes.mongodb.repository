﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using AutoMapper;
using MessyCodes.MongoDb.Sample.AspNetCore.Manager;
using MessyCodes.MongoDb.Sample.AspNetCore.Model;

namespace MessyCodes.MongoDb.Sample.AspNetCore.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProfileController : ControllerBase
    {
        private readonly IProfileManager _profileManager;

        public ProfileController(IProfileManager profileManager)
            => _profileManager = profileManager;

        [HttpGet]
        public async Task<ActionResult<IEnumerable<ProfileDTO>>> Get()
            => (await _profileManager.GetProfile()).ToList();

        [HttpGet("{id}")]
        public async Task<ActionResult<ProfileDTO>> Get(string id)
            => (await _profileManager.GetProfile(id))?.FirstOrDefault();

        [HttpPut]
        public async Task Put(ProfileDTO profile)
            => await _profileManager.AddProfile(profile);

        [HttpDelete]
        public async Task<ActionResult<bool>> Delete(string id)
            => await _profileManager.DeleteProfile(id);

        [HttpPatch]
        public async Task<ActionResult<bool>> Update(ProfileDTO profile)
            => await _profileManager.UpdateProfile(profile);
    }
}