﻿using AutoMapper;
using MessyCodes.MongoDb.Context;
using MessyCodes.MongoDb.Repositories;
using MessyCodes.MongoDb.Sample.AspNetCore.Manager;
using MessyCodes.MongoDb.Sample.AspNetCore.Mapper;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using MongoDB.Driver;
using Swashbuckle.AspNetCore.Swagger;

namespace MessyCodes.MongoDb.Sample.AspNetCore
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            var mongoDbConnectionString = Configuration.GetValue<string>("MongoDb:Host");
            var mongoDbName = Configuration.GetValue<string>("MongoDb:DbName");

            services.AddSingleton<IMongoClient>(_ => new MongoClient(mongoDbConnectionString));
            services.AddTransient<IMongoDatabaseFactory, MongoDatabaseFactory>();
            services.AddTransient<IRepositoryFactory, RepositoryFactory>();
            services.AddSingleton<IDbContext>((_) =>
            {
                var repositoryFactory = _.GetService<IRepositoryFactory>();
                return new DbContext(repositoryFactory, mongoDbName);
            });

            var mappingConfig = new MapperConfiguration(_ =>
            {
                _.AddProfile(new ProfileMapper());
            });

            var mapper = mappingConfig.CreateMapper();
            services.AddSingleton(mapper);

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info { Title = "MongoDb Repository AspNet Core Sample", Version = "v1" });
            });

            services.AddTransient<IProfileManager, ProfileManager>();

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
            
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseHttpsRedirection();

            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "MongoDb Repository AspNet Core Sample");
            });

            app.UseMvc();
            
        }
    }
}
