﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using MessyCodes.MongoDb.Repositories;
using MessyCodes.MongoDb.Sample.AspNetCore.Model;

namespace MessyCodes.MongoDb.Sample.AspNetCore.Mapper
{
    public class ProfileMapper : Profile
    {
        public ProfileMapper()
        {
            // allow bidirectional mapping
            CreateMap<ProfileDTO, Entities.Profile>().ReverseMap();
        }
        
    }
}
