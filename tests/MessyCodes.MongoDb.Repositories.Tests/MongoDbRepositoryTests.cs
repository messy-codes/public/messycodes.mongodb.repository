﻿using FluentAssertions;
using MessyCodes.MongoDb.Repositories.Tests.Helpers;
using MessyCodes.MongoDb.Repositories.Tests.Helpers.Model;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Driver;
using Moq;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace MessyCodes.MongoDb.Repositories.Tests
{
    [TestClass]
    public class MongoDbRepositoryTests
    {
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public async Task Add_OnNullEntity_ShouldThrow_ArgumentNullException()
        {
            var collectionMock = new Mock<IMongoCollection<object>>();
            var mongoDbRepository = new MongoDbRepository<object>(collectionMock.Object);
            await mongoDbRepository.AddAsync(null);
        }

        [TestMethod]
        [ExpectedException(typeof(MongoException))]
        public async Task Add_OnMongoDbFailure_ShouldThrow_MongoException()
        {
            dynamic entity = "test";
            var collectionMock = new Mock<IMongoCollection<object>>();
            collectionMock
                .Setup(stub => stub.InsertOneAsync(It.IsAny<object>(), It.IsAny<InsertOneOptions>(), It.IsAny<CancellationToken>()))
                .Throws(new MongoException(""));

            var mongoDbRepository = new MongoDbRepository<object>(collectionMock.Object);
            await mongoDbRepository.AddAsync(entity);
        }

        [TestMethod]
        public void Add_ShouldVerify_InsertOneAsyncCalled()
        {
            var profile = new Profile() { Username = "abc", Password = "abc" };
            var bsonSerializer = new Mock<IBsonSerializer<Profile>>();
            
            var dbMock = new Mock<IMongoDatabase>();
            var collectionMock = TestHelpers.GetProfileCollectionMock();
            //collectionMock
            //    .Setup(stub => stub.InsertOneAsync(It.IsAny<Profile>(), It.IsAny<InsertOneOptions>(), default(CancellationToken)))
            //    .Verifiable();

            var mongoDbRepository = new MongoDbRepository<Profile>(collectionMock.Object);

            // not sure why when added 'await' or 'GetAwaiter().GetResult()' exception thrown Object reference not set 
            mongoDbRepository.AddAsync(profile);
            collectionMock.Verify(stub => stub.InsertOneAsync(It.IsAny<Profile>(), It.IsAny<InsertOneOptions>(), default(CancellationToken)), Times.AtLeastOnce);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public async Task Get_OnNullFilterDefinition_ShouldThrow_ArgumentNullException()
        {
            var collectionMock = new Mock<IMongoCollection<object>>();
            var mongoDbRepository = new MongoDbRepository<object>(collectionMock.Object);
            await mongoDbRepository.GetAsync(null);
        }

        [TestMethod]
        [ExpectedException(typeof(MongoException))]
        public async Task Get_OnMongoDbFailure_ShouldThrow_MongoException()
        {
            var collectionMock = new Mock<IMongoCollection<Profile>>();
            collectionMock
                .Setup(stub => stub.FindAsync(It.IsAny<FilterDefinition<Profile>>(), default(FindOptions<Profile>), default(CancellationToken)))
                .Throws(new MongoException(""));

            var mongoDbRepository = new MongoDbRepository<Profile>(collectionMock.Object);
            await mongoDbRepository.GetAsync(new BsonDocument());

        }

        [TestMethod]
        public void Get_ShouldReturn_IAsyncCursorOfProfile()
        {
            var profile = new Profile() { Username = "abc", Password = "abc" };
            var profiles = new List<Profile>()
            {
                profile
            };

            var asyncCursorMock = new Mock<IAsyncCursor<Profile>>();
            asyncCursorMock.Setup(_ => _.Current).Returns(profiles);

            var collectionMock = TestHelpers.GetProfileCollectionMock();
            collectionMock
                .Setup(stub => stub.FindAsync(It.IsAny<FilterDefinition<Profile>>(), default(FindOptions<Profile>), default(CancellationToken)))
                .ReturnsAsync(asyncCursorMock.Object);

            var mongoDbRepository = new MongoDbRepository<Profile>(collectionMock.Object);

            var result =  mongoDbRepository.GetAsync(new BsonDocument()).GetAwaiter().GetResult();
            Assert.IsNotNull(result);

            collectionMock
                .Verify(stub => stub.FindAsync(It.IsAny<FilterDefinition<Profile>>(), default(FindOptions<Profile>), default(CancellationToken)),
                Times.Once);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public async Task Remove_OnNullFilterDefinition_ShouldThrow_ArgumentNullException()
        {
            var collectionMock = new Mock<IMongoCollection<object>>();
            var mongoDbRepository = new MongoDbRepository<object>(collectionMock.Object);
            await mongoDbRepository.RemoveAsync(null);
        }

        [TestMethod]
        [ExpectedException(typeof(MongoException))]
        public async Task Remove_OnMongoDbFailure_ShouldThrow_MongoException()
        {
            var collectionMock = new Mock<IMongoCollection<Profile>>();
            collectionMock
                .Setup(stub => stub.DeleteOneAsync(It.IsAny<FilterDefinition<Profile>>(), default(CancellationToken)))
                .Throws(new MongoException(""));

            var mongoDbRepository = new MongoDbRepository<Profile>(collectionMock.Object);
            await mongoDbRepository.RemoveAsync(new BsonDocument());
        }

        [TestMethod]
        public void Remove_ShouldReturn_DeleteResult_True()
        {
            var dbMock = new Mock<IMongoDatabase>();
            var collectionMock = TestHelpers.GetProfileCollectionMock();
            collectionMock
                .Setup(stub => stub.DeleteOneAsync(It.IsAny<FilterDefinition<Profile>>(), default(CancellationToken)))
                .ReturnsAsync(TestHelpers.GetDeleteResult(true, 1).Object);

            var mongoDbRepository = new MongoDbRepository<Profile>(collectionMock.Object);

            var result = mongoDbRepository.RemoveAsync(new BsonDocument()).GetAwaiter().GetResult();
            Assert.IsNotNull(result);
            Assert.IsTrue(result);

            collectionMock
                .Verify(stub => stub.DeleteOneAsync(It.IsAny<FilterDefinition<Profile>>(), default(CancellationToken)),
                Times.Once);
        }

        [TestMethod]
        public void Remove_ShouldReturn_DeleteResult_False_On_IsAcknowledge_Is_False_And_Count_Is_0()
        {
            var dbMock = new Mock<IMongoDatabase>();
            var collectionMock = TestHelpers.GetProfileCollectionMock();
            collectionMock
                .Setup(stub => stub.DeleteOneAsync(It.IsAny<FilterDefinition<Profile>>(), default(CancellationToken)))
                .ReturnsAsync(TestHelpers.GetDeleteResult().Object);

            var mongoDbRepository = new MongoDbRepository<Profile>(collectionMock.Object);

            var result = mongoDbRepository.RemoveAsync(new BsonDocument()).GetAwaiter().GetResult();
            Assert.IsNotNull(result);
            Assert.IsFalse(result);

            collectionMock
                .Verify(stub => stub.DeleteOneAsync(It.IsAny<FilterDefinition<Profile>>(), default(CancellationToken)),
                Times.Once);
        }

        [TestMethod]
        public void Remove_ShouldReturn_DeleteResult_False_On_IsAcknowledge_Is_False_And_Count_Is_1()
        {
            var dbMock = new Mock<IMongoDatabase>();
            var collectionMock = TestHelpers.GetProfileCollectionMock();
            collectionMock
                .Setup(stub => stub.DeleteOneAsync(It.IsAny<FilterDefinition<Profile>>(), default(CancellationToken)))
                .ReturnsAsync(TestHelpers.GetDeleteResult(false, 1).Object);

            var mongoDbRepository = new MongoDbRepository<Profile>(collectionMock.Object);

            var result = mongoDbRepository.RemoveAsync(new BsonDocument()).GetAwaiter().GetResult();
            Assert.IsNotNull(result);
            Assert.IsFalse(result);

            collectionMock
                .Verify(stub => stub.DeleteOneAsync(It.IsAny<FilterDefinition<Profile>>(), default(CancellationToken)),
                Times.Once);
        }

        [TestMethod]
        public void Remove_ShouldReturn_DeleteResult_False_On_IsAcknowledge_Is_True_DeleteCount_Is_0()
        {
            var collectionMock = TestHelpers.GetProfileCollectionMock();
            collectionMock
                .Setup(stub => stub.DeleteOneAsync(It.IsAny<FilterDefinition<Profile>>(), default(CancellationToken)))
                .ReturnsAsync(TestHelpers.GetDeleteResult(true).Object);

            var mongoDbRepository = new MongoDbRepository<Profile>(collectionMock.Object);

            var result = mongoDbRepository.RemoveAsync(new BsonDocument()).GetAwaiter().GetResult();
            Assert.IsNotNull(result);
            Assert.IsFalse(result);

            collectionMock
                .Verify(stub => stub.DeleteOneAsync(It.IsAny<FilterDefinition<Profile>>(), default(CancellationToken)),
                Times.Once);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public async Task RemoveMany_OnNullFilterDefinition_ShouldThrow_ArgumentNullException()
        {
            var collectionMock = new Mock<IMongoCollection<object>>();
            var mongoDbRepository = new MongoDbRepository<object>(collectionMock.Object);
            await mongoDbRepository.RemoveManyAsync(null);
        }

        [TestMethod]
        [ExpectedException(typeof(MongoException))]
        public async Task RemoveMany_OnMongoDbFailure_ShouldThrow_MongoException()
        {
            var collectionMock = new Mock<IMongoCollection<Profile>>();
            collectionMock
                .Setup(stub => stub.DeleteManyAsync(It.IsAny<FilterDefinition<Profile>>(), default(CancellationToken)))
                .Throws(new MongoException(""));

            var mongoDbRepository = new MongoDbRepository<Profile>(collectionMock.Object);
            await mongoDbRepository.RemoveManyAsync(new BsonDocument());
        }

        [TestMethod]
        public void RemoveMany_ShouldReturn_DeleteResult_True()
        {
            var collectionMock = TestHelpers.GetProfileCollectionMock();
            collectionMock
                .Setup(stub => stub.DeleteManyAsync(It.IsAny<FilterDefinition<Profile>>(), default(CancellationToken)))
                .ReturnsAsync(TestHelpers.GetDeleteResult(true, 1).Object);

            var mongoDbRepository = new MongoDbRepository<Profile>(collectionMock.Object);

            var result = mongoDbRepository.RemoveManyAsync(new BsonDocument()).GetAwaiter().GetResult();
            Assert.IsNotNull(result);
            Assert.IsTrue(result);

            collectionMock
                .Verify(stub => stub.DeleteManyAsync(It.IsAny<FilterDefinition<Profile>>(), default(CancellationToken)),
                Times.Once);
        }

        [TestMethod]
        public void RemoveMany_ShouldReturn_DeleteResult_False_On_IsAcknowledge_Is_False_And_Count_Is_0()
        {
            var collectionMock = TestHelpers.GetProfileCollectionMock();
            collectionMock
                .Setup(stub => stub.DeleteManyAsync(It.IsAny<FilterDefinition<Profile>>(), default(CancellationToken)))
                .ReturnsAsync(TestHelpers.GetDeleteResult().Object);

            var mongoDbRepository = new MongoDbRepository<Profile>(collectionMock.Object);

            var result = mongoDbRepository.RemoveManyAsync(new BsonDocument()).GetAwaiter().GetResult();
            Assert.IsNotNull(result);
            Assert.IsFalse(result);

            collectionMock
                .Verify(stub => stub.DeleteManyAsync(It.IsAny<FilterDefinition<Profile>>(), default(CancellationToken)),
                Times.Once);
        }

        [TestMethod]
        public void RemoveMany_ShouldReturn_DeleteResult_False_On_IsAcknowledge_Is_False_And_Count_Is_1()
        {
            var collectionMock = TestHelpers.GetProfileCollectionMock();
            collectionMock
                .Setup(stub => stub.DeleteManyAsync(It.IsAny<FilterDefinition<Profile>>(), default(CancellationToken)))
                .ReturnsAsync(TestHelpers.GetDeleteResult(false,1).Object);

            var mongoDbRepository = new MongoDbRepository<Profile>(collectionMock.Object);

            var result = mongoDbRepository.RemoveManyAsync(new BsonDocument()).GetAwaiter().GetResult();
            Assert.IsNotNull(result);
            Assert.IsFalse(result);

            collectionMock
                .Verify(stub => stub.DeleteManyAsync(It.IsAny<FilterDefinition<Profile>>(), default(CancellationToken)),
                Times.Once);
        }

        [TestMethod]
        public void RemoveMany_ShouldReturn_DeleteResult_False_On_IsAcknowledge_Is_True_DeleteCount_Is_0()
        {
            var collectionMock = TestHelpers.GetProfileCollectionMock();
            collectionMock
                .Setup(stub => stub.DeleteManyAsync(It.IsAny<FilterDefinition<Profile>>(), default(CancellationToken)))
                .ReturnsAsync(TestHelpers.GetDeleteResult(true).Object);

            var mongoDbRepository = new MongoDbRepository<Profile>(collectionMock.Object);

            var result = mongoDbRepository.RemoveManyAsync(new BsonDocument()).GetAwaiter().GetResult();
            Assert.IsNotNull(result);
            Assert.IsFalse(result);

            collectionMock
                .Verify(stub => stub.DeleteManyAsync(It.IsAny<FilterDefinition<Profile>>(), default(CancellationToken)),
                Times.Once);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void Update_OnNullFilterDefinition_ShouldThrow_ArgumentNulException()
        {
            var collectionMock = new Mock<IMongoCollection<object>>();
            var mongoDbRepository = new MongoDbRepository<object>(collectionMock.Object);
            mongoDbRepository.UpdateAsync(null, new BsonDocument()).GetAwaiter().GetResult();
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void Update_OnNullUpdateDefinition_ShouldThrow_ArgumentNulException()
        {
            var collectionMock = new Mock<IMongoCollection<object>>();
            var mongoDbRepository = new MongoDbRepository<object>(collectionMock.Object);
            mongoDbRepository.UpdateAsync(new BsonDocument(), null).GetAwaiter().GetResult();
        }

        [TestMethod]
        [ExpectedException(typeof(MongoException))]
        public void Update_OnMongoDbFailure_ShouldThrow_MongoException()
        {
            var collectionMock = new Mock<IMongoCollection<Profile>>();
            collectionMock
                .Setup(stub => stub.UpdateOneAsync(It.IsAny<FilterDefinition<Profile>>(), 
                    It.IsAny<UpdateDefinition<Profile>>(), 
                    default(UpdateOptions), 
                    default(CancellationToken)))
                .Throws(new MongoException(""));

            var mongoDbRepository = new MongoDbRepository<Profile>(collectionMock.Object);
            mongoDbRepository.UpdateAsync(new BsonDocument(), new BsonDocument()).GetAwaiter().GetResult();
        }

        [TestMethod]
        public void 
            Update_ShouldReturn_UpdateResult_False_On_IsAcknowledgeFalse_IsModifiedCountAvailableFalse_ModifiedCount_0()
        {
            var collectionMock = new Mock<IMongoCollection<Profile>>();
            collectionMock
                .Setup(stub => stub.UpdateOneAsync(It.IsAny<FilterDefinition<Profile>>(),
                    It.IsAny<UpdateDefinition<Profile>>(),
                    default(UpdateOptions),
                    default(CancellationToken)))
                .ReturnsAsync(TestHelpers.GetUpdateResult().Object);

            var mongoDbRespository = new MongoDbRepository<Profile>(collectionMock.Object);
            var result = mongoDbRespository.UpdateAsync(new BsonDocument(), new BsonDocument()).GetAwaiter().GetResult();
            result.Should().BeFalse();

            collectionMock
                .Verify(stub => stub.UpdateOneAsync(It.IsAny<FilterDefinition<Profile>>(),
                    It.IsAny<UpdateDefinition<Profile>>(),
                    default(UpdateOptions),
                    default(CancellationToken)), Times.Once);
        }

        [TestMethod]
        public void
            Update_ShouldReturn_UpdateResult_False_On_IsAcknowledgeFalse_IsModifiedCountAvailableTrue_ModifiedCount_1()
        {
            var collectionMock = new Mock<IMongoCollection<Profile>>();
            collectionMock
                .Setup(stub => stub.UpdateOneAsync(It.IsAny<FilterDefinition<Profile>>(),
                    It.IsAny<UpdateDefinition<Profile>>(),
                    default(UpdateOptions),
                    default(CancellationToken)))
                .ReturnsAsync(TestHelpers.GetUpdateResult(false, true, 1).Object);

            var mongoDbRespository = new MongoDbRepository<Profile>(collectionMock.Object);
            var result = mongoDbRespository.UpdateAsync(new BsonDocument(), new BsonDocument()).GetAwaiter().GetResult();
            result.Should().BeFalse();

            collectionMock
                .Verify(stub => stub.UpdateOneAsync(It.IsAny<FilterDefinition<Profile>>(),
                    It.IsAny<UpdateDefinition<Profile>>(),
                    default(UpdateOptions),
                    default(CancellationToken)), Times.Once);
        }

        [TestMethod]
        public void
            Update_ShouldReturn_UpdateResult_False_On_IsAcknowledgeTrue_IsModifiedCountAvailableFalse_ModifiedCount_1()
        {
            var collectionMock = new Mock<IMongoCollection<Profile>>();
            collectionMock
                .Setup(stub => stub.UpdateOneAsync(It.IsAny<FilterDefinition<Profile>>(),
                    It.IsAny<UpdateDefinition<Profile>>(),
                    default(UpdateOptions),
                    default(CancellationToken)))
                .ReturnsAsync(TestHelpers.GetUpdateResult(true, false, 1).Object);

            var mongoDbRespository = new MongoDbRepository<Profile>(collectionMock.Object);
            var result = mongoDbRespository.UpdateAsync(new BsonDocument(), new BsonDocument()).GetAwaiter().GetResult();
            result.Should().BeFalse();

            collectionMock
                .Verify(stub => stub.UpdateOneAsync(It.IsAny<FilterDefinition<Profile>>(),
                    It.IsAny<UpdateDefinition<Profile>>(),
                    default(UpdateOptions),
                    default(CancellationToken)), Times.Once);
        }

        [TestMethod]
        public void
            Update_ShouldReturn_UpdateResult_False_On_IsAcknowledgeTrue_IsModifiedCountAvailableTrue_ModifiedCount_0()
        {
            var collectionMock = new Mock<IMongoCollection<Profile>>();
            collectionMock
                .Setup(stub => stub.UpdateOneAsync(It.IsAny<FilterDefinition<Profile>>(),
                    It.IsAny<UpdateDefinition<Profile>>(),
                    default(UpdateOptions),
                    default(CancellationToken)))
                .ReturnsAsync(TestHelpers.GetUpdateResult(true, true, 0).Object);

            var mongoDbRespository = new MongoDbRepository<Profile>(collectionMock.Object);
            var result = mongoDbRespository.UpdateAsync(new BsonDocument(), new BsonDocument()).GetAwaiter().GetResult();
            result.Should().BeFalse();

            collectionMock
                .Verify(stub => stub.UpdateOneAsync(It.IsAny<FilterDefinition<Profile>>(),
                    It.IsAny<UpdateDefinition<Profile>>(),
                    default(UpdateOptions),
                    default(CancellationToken)), Times.Once);
        }

        [TestMethod]
        public void
            Update_ShouldReturn_UpdateResult_False_On_IsAcknowledgeTrue_IsModifiedCountAvailableTrue_ModifiedCount_1()
        {
            var collectionMock = new Mock<IMongoCollection<Profile>>();
            collectionMock
                .Setup(stub => stub.UpdateOneAsync(It.IsAny<FilterDefinition<Profile>>(),
                    It.IsAny<UpdateDefinition<Profile>>(),
                    default(UpdateOptions),
                    default(CancellationToken)))
                .ReturnsAsync(TestHelpers.GetUpdateResult(true, true, 1).Object);

            var mongoDbRespository = new MongoDbRepository<Profile>(collectionMock.Object);
            var result = mongoDbRespository.UpdateAsync(new BsonDocument(), new BsonDocument()).GetAwaiter().GetResult();
            result.Should().BeTrue();

            collectionMock
                .Verify(stub => stub.UpdateOneAsync(It.IsAny<FilterDefinition<Profile>>(),
                    It.IsAny<UpdateDefinition<Profile>>(),
                    default(UpdateOptions),
                    default(CancellationToken)), Times.Once);
        }
    }

    
}
