﻿using System;
using System.Collections.Generic;
using System.Text;
using Moq;
using MongoDB.Driver;
using MongoDB.Bson.Serialization;
using MessyCodes.MongoDb.Repositories.Tests.Helpers.Model;

namespace MessyCodes.MongoDb.Repositories.Tests.Helpers
{
    public class TestHelpers
    {
        public static Mock<DeleteResult> GetDeleteResult(bool isAcknowledge = false, int deleteCount = 0)
        {
            var deleteResultMock = new Mock<DeleteResult>();
            deleteResultMock
                .SetupGet(_ => _.IsAcknowledged).Returns(isAcknowledge);
            deleteResultMock
                .SetupGet(_ => _.DeletedCount).Returns(deleteCount);

            return deleteResultMock;
        }

        public static Mock<UpdateResult> GetUpdateResult(
            bool isAcknowledge = false, bool isModifiedCountAvailable = false, long modifiedCount = 0)
        {
            var updateResultMock = new Mock<UpdateResult>();
            updateResultMock
                .SetupGet(_ => _.IsAcknowledged).Returns(isAcknowledge);
            updateResultMock
                .SetupGet(_ => _.IsModifiedCountAvailable).Returns(isModifiedCountAvailable);
            updateResultMock
                .SetupGet(_ => _.ModifiedCount).Returns(modifiedCount);
            return updateResultMock;
        }

        public static Mock<IMongoCollection<Profile>> GetProfileCollectionMock()
        {
            var dbMock = new Mock<IMongoDatabase>();
            var collectionMock = new Mock<IMongoCollection<Profile>>();
            collectionMock.SetupGet(_ => _.Database).Returns(dbMock.Object);
            collectionMock.SetupGet(_ => _.CollectionNamespace).Returns(new CollectionNamespace(new DatabaseNamespace("user"), "Profile"));
            collectionMock.SetupGet(_ => _.Settings).Returns(new MongoCollectionSettings() { });
            collectionMock.SetupGet(_ => _.DocumentSerializer).Returns(BsonSerializer.SerializerRegistry.GetSerializer<Profile>());
            return collectionMock;
        }
    }
}
