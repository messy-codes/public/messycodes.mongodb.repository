﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MessyCodes.MongoDb.Repositories.Tests.Helpers.Model
{
    public class Profile
    {
        public string Username { get; set; }
        public string Password { get; set; }
    }
}
