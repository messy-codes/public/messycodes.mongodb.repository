﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using MongoDB.Driver;
using Moq;
using System;

namespace MessyCodes.MongoDb.Repositories.Tests
{
    [TestClass]
    public class MongoDatabaseFactoryTests
    {
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void Connect_OnNullDbName_ShouldThrow_ArgumentNullException()
        {
            var mongoClientMock = new Mock<IMongoClient>();

            var mongoDatabaseFactory = new MongoDatabaseFactory(mongoClientMock.Object);
            mongoDatabaseFactory.Connect(null);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void Connect_OnEmptyDbName_ShouldThrow_ArgumentNullException()
        {
            var mongoClientMock = new Mock<IMongoClient>();

            var mongoDatabaseFactory = new MongoDatabaseFactory(mongoClientMock.Object);
            mongoDatabaseFactory.Connect(string.Empty);
        }

        [TestMethod]
        [ExpectedException(typeof(MongoException))]
        public void Connect_OnFailedMongoClient_ShouldThrow_MongoException()
        {
            var mongoClientMock = new Mock<IMongoClient>();
            mongoClientMock
                .Setup(stub => stub.GetDatabase(It.IsAny<string>(), It.IsAny<MongoDatabaseSettings>()))
                .Throws(new MongoException(""));

            var mongoDatabaseFactory = new MongoDatabaseFactory(mongoClientMock.Object);
            mongoDatabaseFactory.Connect("db");
        }

        [TestMethod]
        public void Connect_ShouldReturn_InstanceOfMongoDatabase()
        {
            var dbMock = new Mock<IMongoDatabase>();
            var mongoClientMock = new Mock<IMongoClient>();
            mongoClientMock
                .Setup(stub => stub.GetDatabase(It.IsAny<string>(), It.IsAny<MongoDatabaseSettings>()))
                .Returns(dbMock.Object);
;
            var mongoDatabaseFactory = new MongoDatabaseFactory(mongoClientMock.Object);
            var result = mongoDatabaseFactory.Connect("db");

            Assert.IsNotNull(result);

            mongoClientMock
               .Verify(stub => stub.GetDatabase(It.IsAny<string>(), It.IsAny<MongoDatabaseSettings>()), Times.Once);
        }
    }
}
