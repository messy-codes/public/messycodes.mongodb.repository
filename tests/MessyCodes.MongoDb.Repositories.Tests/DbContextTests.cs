﻿using FluentAssertions;
using MessyCodes.MongoDb.Context;
using MessyCodes.MongoDb.Entities;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;

namespace MessyCodes.MongoDb.Repositories.Tests
{
    [TestClass]
    public class DbContextTests
    {
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void DbContext_OnNullOnRepositoryFactory_ShouldThrow_ArgumentNullException()
        {
            var dbContext = new DbContext(null, "MessyCode");
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void DbContext_OnNullOnDbName_ShouldThrow_ArgumentNullException()
        {
            var repositoryFactoryMock = new Mock<IRepositoryFactory>();
            var dbContext = new DbContext(repositoryFactoryMock.Object, null);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void DbContext_OnNullEmptyDbName_ShouldThrow_ArgumentNullException()
        {
            var repositoryFactoryMock = new Mock<IRepositoryFactory>();
            var dbContext = new DbContext(repositoryFactoryMock.Object, string.Empty);
        }

        [TestMethod]
        public void DbContext_Initialize_ProfileRepository_ShouldNotNull()
        {
            var repositoryMock = new Mock<IRepository<Profile>>();
            var repositoryFactoryMock = new Mock<IRepositoryFactory>();
            repositoryFactoryMock
                .Setup(stub => stub.Create<Profile>(It.IsAny<string>(), It.IsAny<string>()))
                .Returns(repositoryMock.Object);

            var dbContext = new DbContext(repositoryFactoryMock.Object, "MessyCodes");
            dbContext.ProfileRepository.Should().NotBeNull();

            repositoryFactoryMock
                .Verify(stub => stub.Create<Profile>(It.IsAny<string>(), It.IsAny<string>()), Times.Once);

        }
    }
}
