﻿using FluentAssertions;
using MessyCodes.MongoDb.Repositories.Tests.Helpers;
using MessyCodes.MongoDb.Repositories.Tests.Helpers.Model;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MongoDB.Bson;
using MongoDB.Driver;
using Moq;
using System;
using System.Threading;

namespace MessyCodes.MongoDb.Repositories.Tests
{
    [TestClass]
    public class MongoDbAggregateRepositoryTests
    {
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void Count_NullFilterDefinition_ShouldThrow_ArgumentNullException()
        {
            var collectionMock = new Mock<IMongoCollection<object>>();
            var mongoDbRepository = new MongoDbAggregateRepository<object>(collectionMock.Object);
            mongoDbRepository.CountAsync(null).GetAwaiter().GetResult();
        }

        [TestMethod]
        [ExpectedException(typeof(MongoException))]
        public void Count_OnMongoDbFailure_ShouldThrow_MongoException()
        {
            var collectionMock = new Mock<IMongoCollection<Profile>>();
            collectionMock
                .Setup(stub => stub.CountDocumentsAsync(It.IsAny<FilterDefinition<Profile>>(), default(CountOptions), default(CancellationToken)))
                .Throws(new MongoException(""));

            var mongoDbRepository = new MongoDbAggregateRepository<Profile>(collectionMock.Object);
            mongoDbRepository.CountAsync(new BsonDocument()).GetAwaiter().GetResult();
        }

        [TestMethod]
        public void Count_ShouldReturn_TotalNumberOfDocument()
        {
            var collectionMock = TestHelpers.GetProfileCollectionMock();
            collectionMock
                .Setup(stub => stub.CountDocumentsAsync(It.IsAny<FilterDefinition<Profile>>(), default(CountOptions), default(CancellationToken)))
                .ReturnsAsync(1);

            var mongoDbRepository = new MongoDbAggregateRepository<Profile>(collectionMock.Object);
            var result = mongoDbRepository.CountAsync(new BsonDocument()).GetAwaiter().GetResult();

            result.Should().Be(1);

            collectionMock
                .Verify(stub => stub.CountDocumentsAsync(It.IsAny<FilterDefinition<Profile>>(), default(CountOptions), default(CancellationToken)), Times.Once);
        }
    }
}
