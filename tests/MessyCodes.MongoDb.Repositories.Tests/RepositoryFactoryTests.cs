using Microsoft.VisualStudio.TestTools.UnitTesting;
using MongoDB.Driver;
using Moq;
using System;

namespace MessyCodes.MongoDb.Repositories.Tests
{
    [TestClass]
    public class RepositoryFactoryTests
    {

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void RepositoryFactory_OnNullDbName_ShouldThrow_ArgumentNullException()
        {
            var mongoDbFactory = new Mock<IMongoDatabaseFactory>();
            mongoDbFactory
                .Setup(stub => stub.Connect(It.IsAny<string>()))
                .Throws<ArgumentNullException>();

            var repositoryFactory = new RepositoryFactory(mongoDbFactory.Object);
            var result = repositoryFactory.Create<object>(null, "user");
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void RepositoryFactory_OnEmptyDbName_ShouldThrow_ArgumentNullException()
        {
            var mongoDbFactory = new Mock<IMongoDatabaseFactory>();
            mongoDbFactory
                .Setup(stub => stub.Connect(It.IsAny<string>()))
                .Throws<ArgumentNullException>();

            var repositoryFactory = new RepositoryFactory(mongoDbFactory.Object);
            var result = repositoryFactory.Create<object>(string.Empty, "user");
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void RepositoryFactory_OnNullCollectionName_ShouldThrow_ArgumentNullException()
        {
            var mongoDbFactory = new Mock<IMongoDatabaseFactory>();
            mongoDbFactory
                .Setup(stub => stub.Connect(It.IsAny<string>()))
                .Throws<ArgumentNullException>();

            var repositoryFactory = new RepositoryFactory(mongoDbFactory.Object);
            var result = repositoryFactory.Create<object>("db",null);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void RepositoryFactory_OnCollectionName_ShouldThrow_ArgumentNullException()
        {
            var mongoDbFactory = new Mock<IMongoDatabaseFactory>();
            mongoDbFactory
                .Setup(stub => stub.Connect(It.IsAny<string>()))
                .Throws<ArgumentNullException>();

            var repositoryFactory = new RepositoryFactory(mongoDbFactory.Object);
            var result = repositoryFactory.Create<object>("db", string.Empty);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void RepositoryFactory_OnNullDbFactory_ShouldThrow_ArgumentNullException()
        {
            IMongoDatabaseFactory dbFactoryMock = null;

            var repositoryFactory = new RepositoryFactory(dbFactoryMock);
        }

        [TestMethod]
        public void RepositoryFactory_OnCreate_ShouldReturn_InstanceOf_MongoDbRepositoryOfObject()
        {
            var collectionMock = new Mock<IMongoCollection<object>>();

            var dbMock = new Mock<IMongoDatabase>();
            dbMock
                .Setup(stub => stub.GetCollection<object>(It.IsAny<string>(), It.IsAny<MongoCollectionSettings>()))
                .Returns(collectionMock.Object);

            var mongoDbFactory = new Mock<IMongoDatabaseFactory>();
            mongoDbFactory
                .Setup(x => x.Connect(It.IsAny<string>()))
                .Returns(dbMock.Object);

            var repositoryFactory = new RepositoryFactory(mongoDbFactory.Object);
            var result = repositoryFactory.Create<object>("db", "user");
            Assert.IsNotNull(result);

            mongoDbFactory
                .Verify(x => x.Connect(It.IsAny<string>()), Times.Once);
        }
    }
}
