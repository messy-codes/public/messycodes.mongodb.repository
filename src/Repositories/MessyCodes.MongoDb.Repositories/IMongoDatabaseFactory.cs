﻿using MongoDB.Driver;

namespace MessyCodes.MongoDb.Repositories
{
    public interface IMongoDatabaseFactory
    {
        IMongoDatabase Connect(string dbName);
    }
}
