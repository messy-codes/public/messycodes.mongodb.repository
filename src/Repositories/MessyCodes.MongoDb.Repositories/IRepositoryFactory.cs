﻿namespace MessyCodes.MongoDb.Repositories
{
    public interface IRepositoryFactory
    {
        IRepository<TEntity> Create<TEntity>(string dbName, string collectionName);
    }
}
