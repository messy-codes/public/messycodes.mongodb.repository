﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Driver;

namespace MessyCodes.MongoDb.Repositories
{
    public class MongoDbAggregateRepository<TEntity> :
        IAggregateRepository<TEntity>
    {
        private readonly IMongoCollection<TEntity> _collection;
        public MongoDbAggregateRepository(IMongoCollection<TEntity> collection) => _collection = collection;

        public async Task<long> CountAsync(FilterDefinition<TEntity> filterDefinition)
        {
            if (filterDefinition == null) throw new ArgumentNullException("FilterDefinition should not be null");
            try
            {
                return await _collection.CountDocumentsAsync(filterDefinition);
            }catch(MongoException exception)
            {
                throw new MongoException(
                    $"Exception thrown while trying to performs [RemoveManyAsync] with exception {exception}");
            }
        }

    }
}
