﻿using MongoDB.Driver;
using System;

namespace MessyCodes.MongoDb.Repositories
{
    public class MongoDatabaseFactory : IMongoDatabaseFactory
    {
        private readonly IMongoClient _mongoClient;
        public MongoDatabaseFactory(IMongoClient mongoClient) => _mongoClient = mongoClient;
        public IMongoDatabase Connect(string dbName)
        {
            if (string.IsNullOrEmpty(dbName)) throw new ArgumentNullException("");

            try
            {
                return _mongoClient.GetDatabase(dbName);
            }
            catch (MongoException exception)
            {
                throw new MongoException($"Failed to initialized mongodb client {exception}");
            }
        }
    }
}
