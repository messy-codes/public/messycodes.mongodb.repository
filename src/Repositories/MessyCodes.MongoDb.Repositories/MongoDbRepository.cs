﻿using MongoDB.Driver;
using System;
using System.Threading.Tasks;

namespace MessyCodes.MongoDb.Repositories
{
    public class MongoDbRepository<TEntity> : IRepository<TEntity>
    {
        private readonly IMongoCollection<TEntity> _collection;
        public MongoDbRepository(IMongoCollection<TEntity> collection) => _collection = collection;

        public async Task AddAsync(TEntity entity)
        {
            if (entity == null) throw new ArgumentNullException("");

            try
            {
                await _collection.InsertOneAsync(entity);
            }
            catch (MongoException exception)
            {
                throw new MongoException(exception.ToString());
            }
        }

        public async Task<IAsyncCursor<TEntity>> GetAsync(FilterDefinition<TEntity> filterDefinition)
        {
            if (filterDefinition == null) throw new ArgumentNullException("FilterDefinition should not be null");

            try
            {
                return await _collection.FindAsync(filterDefinition);
            }
            catch (MongoException exception)
            {
                throw new MongoException(exception.ToString());
            }
        }

        public async Task<bool> RemoveAsync(FilterDefinition<TEntity> filterDefinition)
        {
            if (filterDefinition == null) throw new ArgumentNullException("FilterDefinition should not be null");

            try
            {
                var result = await _collection.DeleteOneAsync(filterDefinition);
                return (result.IsAcknowledged && result.DeletedCount > 0);
            }
            catch (MongoException exception)
            {
                throw new MongoException(exception.ToString());
            }
        }

        public async Task<bool> RemoveManyAsync(FilterDefinition<TEntity> filterDefinition)
        {
            if (filterDefinition == null) throw new ArgumentNullException("FilterDefinition should not be null");

            try
            {
                var result = await _collection.DeleteManyAsync(filterDefinition);
                return (result.IsAcknowledged && result.DeletedCount > 0);
            }catch(MongoException exception)
            {
                throw new MongoException($"Exception thrown while trying to performs [RemoveManyAsync] with exception {exception}");
            }
        }

        public async Task<bool> UpdateAsync(FilterDefinition<TEntity> filterDefinition, UpdateDefinition<TEntity> updateDefinition)
        {
            if (filterDefinition == null || updateDefinition == null)
                throw new ArgumentNullException("Either FilterDefinition or UpdateDefinition should not be null");
            try
            {
                var updateResult = await _collection.UpdateOneAsync(filterDefinition, updateDefinition);
                return (updateResult.IsAcknowledged && updateResult.IsModifiedCountAvailable && updateResult.ModifiedCount > 0);
            }
            catch(MongoException exception)
            {
                throw new MongoException($"Exception thrown while trying to performs [UpdateAsync] with exception {exception}");
            }
           
        }
    }
}
