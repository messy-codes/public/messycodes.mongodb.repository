﻿using MongoDB.Driver;
using System.Threading.Tasks;

namespace MessyCodes.MongoDb.Repositories
{
    public interface IRepository<TEntity>
    {
        Task AddAsync(TEntity entity);
        Task<IAsyncCursor<TEntity>> GetAsync(FilterDefinition<TEntity> filterDefinition);
        Task<bool> RemoveAsync(FilterDefinition<TEntity> filterDefinition);
        Task<bool> RemoveManyAsync(FilterDefinition<TEntity> filterDefinition);
        Task<bool> UpdateAsync(FilterDefinition<TEntity> filterDefinition, UpdateDefinition<TEntity> updateDefinition);
    }
}
