﻿using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace MessyCodes.MongoDb.Repositories
{
    public interface IAggregateRepository<TEntity>
    {
        Task<long> CountAsync(FilterDefinition<TEntity> filterDefinition);
    }
}
