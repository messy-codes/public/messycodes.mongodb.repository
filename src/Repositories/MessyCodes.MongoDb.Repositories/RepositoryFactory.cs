﻿using System;

namespace MessyCodes.MongoDb.Repositories
{
    public class RepositoryFactory : IRepositoryFactory
    {
        private readonly IMongoDatabaseFactory _dbFactory;
        public RepositoryFactory(IMongoDatabaseFactory dbFactory)
        {
            _dbFactory = dbFactory ?? throw new ArgumentNullException("Database factory object are not being initialized");
        }

        public IRepository<TEntity> Create<TEntity>(string dbName, string collectionName)
        {
            if (string.IsNullOrEmpty(dbName) || string.IsNullOrEmpty(collectionName))
                throw new ArgumentNullException();

            var database = _dbFactory.Connect(dbName);
            return new MongoDbRepository<TEntity>(database.GetCollection<TEntity>(collectionName));
        }
    }
}
