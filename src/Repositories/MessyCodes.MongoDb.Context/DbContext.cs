﻿using MessyCodes.MongoDb.Entities;
using MessyCodes.MongoDb.Repositories;
using System;

namespace MessyCodes.MongoDb.Context
{
    public class DbContext : IDbContext
    {
        public DbContext(IRepositoryFactory repositoryFactory, string dbName)
        {
            if (repositoryFactory == null || string.IsNullOrEmpty(dbName))
                throw new ArgumentNullException("RepositoryFactory should not be null");

            ProfileRepository = repositoryFactory.Create<Profile>(dbName, typeof(Profile).Name);
        }

        public IRepository<Profile> ProfileRepository { get; }
    }
}
