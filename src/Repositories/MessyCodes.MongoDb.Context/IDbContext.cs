﻿using MessyCodes.MongoDb.Entities;
using MessyCodes.MongoDb.Repositories;

namespace MessyCodes.MongoDb.Context
{
    public interface IDbContext
    {
        IRepository<Profile> ProfileRepository { get; }        
    }
}
